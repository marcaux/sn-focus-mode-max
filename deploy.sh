#!/bin/sh
apt update
apt install zip
zip -r sn-focus-mode-max.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-focus-mode-max.zip public/
cd public
unzip sn-focus-mode-max.zip
