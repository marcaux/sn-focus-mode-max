# Focus Mode Max

A quick toggle to colorize various elements with the Standard Notes Super Editor to help distinguishing between various formatting options. It should be available within the official plugins repository.

This Toggle is theme agnostic.

If you always want to use the latest version (I sometimes tend to adjust a few little things here and there), you can use my deployed version here:

https://marcaux.gitlab.io/sn-focus-mode-max/ext.json

![Screenshot](https://marcaux.gitlab.io/sn-focus-mode-max/screenshot.png "Screenshot")
